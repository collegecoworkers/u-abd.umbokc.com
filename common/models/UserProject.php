<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class UserProject extends ActiveRecord {

	public static function tableName() {
		return '{{%user_project}}';
	}

	public function rules() {
		return [
		];
	}

	public function attributeLabels() {
		return [
			'user_id' => 'Пользователь',
			'project_id' => 'Проект',
		];
	}

	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

	public function getProject() {
		return Project::findId($this->project_id);
	}

	public function getUser() {
		return User::findIdentity($this->user_id);
	}
}
