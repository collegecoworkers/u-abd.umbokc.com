<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Project extends ActiveRecord {

	public static function tableName() {
		return '{{%project}}';
	}

	public function attributeLabels() {
		return [
			'title' => 'Название',
			'desc' => 'Описание',
			'date' => 'Дата создания',
		];
	}

	public static function findId($id) {
		return static::findOne(['id' => $id]);
	}

	public static function getAllNav($route) {
		$project = [];
		$all_project = self::find()->all();

		for ($i=0; $i < count($all_project); $i++) {
			$item = $all_project[$i];
			$project[] = [
				'label' => $item->title,
				'icon' => 'fa fa-trello',
				'url' => ["/site/project?id=$item->id"],
				'active' => ($route == 'site/project' and $_GET['id'] == $item->id)
			];
		}

		return $project;
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

	public function thisIsJoin() {
		return UserProject::findOne(['user_id' => \Yii::$app->user->identity->id, 'project_id' => $this->id]);
	}

	public function countUsers() {
		return UserProject::find()->where(['project_id' => $this->id])->count();
	}

	public function countPurposes() {
		return Purpose::find()->where(['project_id' => $this->id])->count();
	}

}
