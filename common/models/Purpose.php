<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

use common\models\Folder;

class Purpose extends ActiveRecord {

	public static function tableName() {
		return '{{%purpose}}';
	}

	public function rules() {
		return [
			[['name'], 'string'],
			[['status'], 'integer'],
		];
	}

	public function attributeLabels() {
		return [
			'name' => 'Название',
			'desc' => 'Опиисание',
			'status' => 'Статус',
			'date' => 'Дата',
			'project_id' => 'Проект',
		];
	}

	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

	public function savePurpose(){
		return $this->save();
	}

	public function getStatus(){
		switch ($this->status) {
			case 1: return 'Готов';
			case 0: return 'Не готов';
			default: return 'Ошибка';
		}
	}

	public static function allStatuses(){
		$statuses = [];
		$statuses[0] = 'Не готов';
		$statuses[1] = 'Готов';
		return $statuses;
	}

}
