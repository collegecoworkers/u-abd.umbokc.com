<?php

use adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\{
	Project,
	User
};

$route = $this->context->route;

$project = Project::getAllNav($route);

$project[] = [ 'label' => 'Добавить задачу', 'icon' => 'fa fa-plus',  'url' => ['/site/add-project'], 'active' => $route == 'site/add-project' ];

$items = [
	['label' => 'Меню', 'options' => ['class' => 'header']],
	[ 'label' => 'Главная', 'icon' => 'fa fa-home',  'url' => ['/'], 'active' => $route == 'site/index']
];

if (User::isWorker()){
	$items[] = ['label' => 'Мои задачи', 'icon' => 'fa fa-trello', 'url' => ["/site/my"], 'active' => $route == 'site/my']; 
} else {
	$items[] = [ 'label' => 'Задачи', 'icon' => 'fa fa-trello', 'url' => '#', 'items' => $project]; 
}


?>
<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<span style="width: 45px;height: 45px;background: #ececec;display: block;line-height: 45px;color: #333030;text-align: center;border-radius: 50%;font-size: 22px;">
					<?= ucfirst(\Yii::$app->user->identity->username[0]) ?>
				</span>
			</div>
			<div class="pull-left info">
				<p><?= \Yii::$app->user->identity->username ?></p>
				<a href=""><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<?=
			Menu::widget(
				[
					'options' => ['class' => 'sidebar-menu'],
					'items' => $items,
				]
			)
		?>
	</section>
</aside>
