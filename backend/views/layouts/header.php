<?php
use yii\helpers\Html;
?>
<header class="main-header">
	<!-- Logo -->
	<a href="/" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>EP</b></span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>Enterprise</b></span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="/" class="dropdown-toggle" data-toggle="dropdown">
						<span style="margin-right: 10px;margin-top: -2px;width: 25px;height: 25px;background: #ececec;display: inline-block;line-height: 25px;color: #333030;text-align: center;border-radius: 50%;font-size: 12px;">
							<?= ucfirst(\Yii::$app->user->identity->username[0]) ?>
						</span>
						<span class="hidden-xs"><?= \Yii::$app->user->identity->username ?></span>
					</a>
					<ul class="dropdown-menu">
						<li class="user-footer">
							<div class="pull-right">
								<a href="/admin/site/logout" class="btn btn-default btn-flat">Выход</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
