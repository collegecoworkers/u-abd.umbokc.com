<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
	<div class="row">
		<div class="login-box" style="width: 700px; margin-top: 13%">
			<div class="col-md-12 box box-radius" style="padding: 60px 120px; text-align: center;" >
			<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

				<?= $form->field($model, 'username', ['template' => '
						<div class="col-sm-12" style="margin-top:15px;">
							<div class="input-group col-sm-12">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-user"></span>
								</span>
								{input}
							</div>{error}{hint}
						</div>'])->textInput(['autofocus' => true])
								->input('text', ['placeholder'=>'Логин']) ?>

				<?= $form->field($model, 'email', ['template' => '
						<div class="col-sm-12" style="margin-top:15px;">
							<div class="input-group col-sm-12">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-user"></span>
								</span>
								{input}
							</div>{error}{hint}
						</div>'])->textInput()
								->input('text', ['placeholder'=>'email']) ?>

				<?= $form->field($model, 'password', ['template' => '
						<div class="col-sm-12" style="margin-top:15px;">
							<div class="input-group col-sm-12">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-lock"></span>
								</span>
								{input}
							</div>{error}{hint}
						</div>'])->passwordInput()
								->input('password', ['placeholder'=>'Пароль'])?>

				<div class="form-group">
					<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'login-button', 'style' => 'margin-top: 10px']) ?>
					<hr style="margin: 10px 0 5px;">
					<?= Html::a('Войти', ['/site/login'],['class' => 'btn']) ?>
				</div>

			<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
