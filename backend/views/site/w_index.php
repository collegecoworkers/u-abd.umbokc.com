<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\User;

$this->title = Yii::t('app', 'Новые задачи');
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
	'query' => $model,
	'pagination' => [
	 'pageSize' => 20,
	],
]);

?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">

<div class="contact-index">
	<?php
	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
		'columns' => [
			[
				'label' => 'Название',
				'format' => 'raw',
				'attribute' => 'title',
				'value' => function($data){
					return Html::a($data->title, ['site/project', 'id'=>$data->id ]);
				}
			],
			[
				'label' => 'Описание',
				'attribute' => 'desc',
				'value' => function($data){
					return substr($data->desc, 0, 100) . (strlen($data->desc) > 100 ? '...' : '');
				}
			],
			[
				'label' => 'Шагов',
				'value' => function($data){
					return $data->countPurposes();
				}
			],
			[
				'label' => 'Сотрудников',
				'value' => function($data){
					return $data->countUsers();
				}
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'header'=>'Действия', 
				'headerOptions' => ['width' => '200'],
				'template' => '{join}',
				'buttons' => [
					'join' => function ($url, $model) {
						if ($model->thisIsJoin() == null) {
							return Html::a('<span class="glyphicon glyphicon-plus"></span> Присоедениться', ['site/join-to-project', 'id'=>$model->id ], [
								'title' => Yii::t('app', 'lead-update'),
							]);
						}
					},
				],
			],
		],
	]);
	?>

</div>

		</div>
	</div>
</div>
