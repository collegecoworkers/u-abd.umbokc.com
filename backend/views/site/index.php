<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\User;

$this->title = Yii::t('app', 'Новые задачи');
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
	'query' => $model,
	'pagination' => [
	 'pageSize' => 20,
	],
]);

?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">

<div class="contact-index">
		<?= Html::a(Yii::t('app','Добавить'), Url::base() . 'site/add-project') ?>
	<div class="fa-br"></div>
	<br>
	<?php
	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
		'columns' => [
			'title',
			[
				'label' => 'Описание',
				'attribute' => 'desc',
				'value' => function($data){
					return $data->desc;
					// return substr($data->desc, 0, 100) . (strlen($data->desc) > 100 ? '...' : '');
				}
			],
			[
				'label' => 'Шагов',
				'value' => function($data){
					return $data->countPurposes();
				}
			],
			[
				'label' => 'Сотрудников',
				'value' => function($data){
					return $data->countUsers();
				}
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'header'=>'Действия', 
				'headerOptions' => ['width' => '80'],
				'template' => '{view} {update} {delete}',
				'buttons' => [
					'view' => function ($url, $model) {
						return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['site/project', 'id'=>$model->id ], [
							'title' => Yii::t('app', 'lead-view'),
						]);
					},
					'update' => function ($url, $model) {
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['site/update-project', 'id'=>$model->id ], [
							'title' => Yii::t('app', 'lead-update'),
						]);
					},
					'delete' => function ($url, $model) {
						return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['site/delete-project', 'id'=>$model->id ], [
							'title' => Yii::t('app', 'lead-delete'),
						]);
					}

				],
			],
		],
	]);
	?>

</div>

		</div>
	</div>
</div>
