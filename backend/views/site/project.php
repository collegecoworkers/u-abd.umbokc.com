<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\User;

$this->title = Yii::t('app', 'Шаги');
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
	'query' => $model,
	'pagination' => [
	 'pageSize' => 20,
	],
]);

$dataProvider2 = new ActiveDataProvider([
	'query' => $model2,
	'pagination' => [
	 'pageSize' => 20,
	],
]);

?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading"><?= $this->title ?></div>
			<div class="panel-body">
				<div class="progress-group">
					<span class="progress-text">Готово шагов</span>
					<span class="progress-number"><b><?= $done ?></b>/<?= $all ?></span>

					<div class="progress sm">
						<div class="progress-bar progress-bar-aqua" style="width:<?= ($all == 0 ? 0 : floor($done/$all * 100)) . '%'; ?>"></div>
					</div>
				</div>
				<div class="contact-index">
					<?= Html::a(Yii::t('app','Добавить шаг'), ['/site/create', 'id' => $_GET['id']]) ?>
					<div class="fa-br"></div>
					<br>
					<?php
					echo GridView::widget([
						'dataProvider' => $dataProvider,
						'layout' => "{items}\n{pager}",
						'columns' => [
							'name',
							[
								'label' => 'Описание',
								'attribute' => 'desc',
								'value' => function($data){
									return $data->desc;
									// return substr($data->desc, 0, 100) . (strlen($data->desc) > 100 ? '...' : '');
								}
							],
							[
								'label' => 'Статус',
								'attribute' => 'status',
								'value' => function($data){
									return $data->getStatus();
								}
							],
							[
								'class' => 'yii\grid\ActionColumn',
								'header'=>'Действия', 
								'headerOptions' => ['width' => '80'],
								'template' => '{update} {delete}',
							],
						],
					]);
					?>

				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">Пользователи</div>
			<div class="panel-body">
				<div class="contact-index">
					<?php
					echo GridView::widget([
						'dataProvider' => $dataProvider2,
						'layout' => "{items}\n{pager}",
						'columns' => [
							'username',
							'email',
						],
					]);
					?>

				</div>
			</div>
		</div>
	</div>
</div>
