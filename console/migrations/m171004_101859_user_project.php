<?php

use yii\db\Migration;

class m171004_101859_user_project extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_project', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'project_id' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%user_project}}');
    }
}
